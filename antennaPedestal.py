import socket
import crc16
import time
import sys

# This is the default IP of the pedestal
IP = "192.168.0.244"
port = 1983

# This is the local IP of the machine interfacing with the pedestal
IP_local = "192.168.0.12"

############################################################

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind((IP_local, 20014))

############################################################

def printHelp():
    print('----------------------------------------------------------------------------------------')
    print('NOTE: - Please ensure that you are using the correct IP')
    print('      - Commands are executed in the order set')
    print('----------------------------------------------------------------------------------------')
    print('-h -- Help')
    print('-setaz <000.00> -- Set the azimuth position')
    print('-setel <000.00> -- Set the elevation position')
    print('-getaz -- Get the current azimuth position')
    print('-getel -- Get the current elevation position')
    print('-gps -- Get the GPS lat, lon, alt')
    print('-deg -- Get the heading in degrees (TN)')
    print('-s -- Get the serial number')
    print('-lat <LLMMSS> -- Point system to a target latitude')
    print('-lon <LLLMMSS> -- Point system to a target longitude')
    print('-alt <0100> -- Point system to a target altitude')
    print('-c -- Run compas calibration')
    print('-speed <01> -- Set the motor speed (1-10)')
    print('-stepaz <-90> <90> <1> <1> -- Step from A to B in steps of C with a dwell time of D')
    print('-stepel <0> <90> <1> <1> -- Step from A to B in steps of C with a dwell time of D')
    print('-stow -- Stow the system')
    print('-aux -- Gets the aux data from the system')
    print('')
    print('Example: python antennaPedestal.py -gps -stepaz -10 10 1 0.5')
    print('----------------------------------------------------------------------------------------')
            
def NMT_SerialEncode(stringIn):
    calc_crc = crc16.crc16xmodem(stringIn.encode('utf-8'))
    return (stringIn + format(calc_crc,'04x') + '\r\n').encode('utf-8')

def setTargetLat(lat, IP, port):
    message = NMT_SerialEncode('>TGTLA,AA,' + lat + ',XXXX')
    sock.sendto(message,(IP, port))

def setTargetLon(lon, IP, port):
    message = NMT_SerialEncode('>TGTLO,AA,' + lon + ',XXXX')
    sock.sendto(message,(IP, port))

def setTargetAlt(alt, IP, port):
    message = NMT_SerialEncode('>TGTAL,AA,' + alt + ',XXXX')
    sock.sendto(message,(IP, port))

def stopPositioner(IP, port):
    message = NMT_SerialEncode('>MSTOP,AA,XXXX')
    sock.sendto(message,(IP, port))

def stow(IP, port):
    message = NMT_SerialEncode('>STOW,AA,XXXX')
    sock.sendto(message,(IP, port))

def stop(IP, port):
    message = NMT_SerialEncode('>MSTOP,AA,XXXX')
    sock.sendto(message,(IP, port))
    temp, addr = sock.recvfrom(1024)

def setSpeed(speed, IP, port):
    message = NMT_SerialEncode('>SSPEED,AA,' + speed + ',XXXX')
    sock.sendto(message,(IP, port))
    speed, addr = sock.recvfrom(1024)

def calibrateCompas(IP, port):
    message = NMT_SerialEncode('>CCAL,AA,XXXX')
    sock.sendto(message,(IP, port))
    # <CCAL,AA,Y,XXXX\r\n
    # Y - 1 = Success, 0 = Fail
    calibration, addr = sock.recvfrom(1024)
    calibration = int(calibration[9:10].decode('ascii'))
    if calibration == 1:
        calibration = 'Success!'
    else:
        calibration = 'Failure'
    return calibration
    
def setAzPosition(Az, IP, port):
    message = NMT_SerialEncode('>CMDAZ,99,' + Az + ',')
    # Set position
    sock.sendto(message,(IP, port))
    AzPosition, addr = sock.recvfrom(1024)
    message = NMT_SerialEncode('?AZP,AA,XXXX')
    sock.sendto(message,(IP, port))
    # Get position
    # <ELPOS,AA,-BBB.BB,XXXX\r\n
    # -BBB.BB - The current azimuth (in the pedestal reference frame) of the positioner
    AzPosition, addr = sock.recvfrom(1024)
    AzPosition = float(AzPosition[10:16].decode('ascii'))
    return AzPosition

def getAzPosition(IP, port):
    message = NMT_SerialEncode('?AZP,AA,XXXX')
    sock.sendto(message,(IP, port))
    # Get position
    # <ELPOS,AA,-BBB.BB,XXXX\r\n
    # -BBB.BB - The current elevation (in the pedestal reference frame) of the positioner
    AzPosition, addr = sock.recvfrom(1024)
    AzPosition = float(AzPosition[10:16].decode('ascii'))
    return AzPosition

def setElPosition(El, IP, port):
    message = NMT_SerialEncode('>CMDEL,99,' + El + ',')
    # Set position
    sock.sendto(message,(IP, port))
    ElPosition, addr = sock.recvfrom(1024)
    message = NMT_SerialEncode('?ELP,AA,XXXX')
    sock.sendto(message,(IP, port))
    # Get position
    # <ELPOS,AA,-BBB.BB,XXXX\r\n
    # -BBB.BB - The current elevation (in the pedestal reference frame) of the positioner
    ElPosition, addr = sock.recvfrom(1024)
    ElPosition = float(ElPosition[10:16].decode('ascii'))
    return ElPosition

def getElPosition(IP, port):
    message = NMT_SerialEncode('?ELP,AA,XXXX')
    sock.sendto(message,(IP, port))
    # Get position
    # <ELPOS,AA,-BBB.BB,XXXX\r\n
    # -BBB.BB - The current elevation (in the pedestal reference frame) of the positioner
    ElPosition, addr = sock.recvfrom(1024)
    ElPosition = float(ElPosition[10:16].decode('ascii'))
    return ElPosition

def getGPS(IP, port):
    message = NMT_SerialEncode('?GPS,AA,XXXX')
    sock.sendto(message,(IP, port))
    # <GPSLA,AA,-DDMMSS,XXXX\r\n
    # LAT -DDMMSS (Degrees [-90:90] Minutes [00:60] Seconds [00:60])
    lat, addr = sock.recvfrom(1024)
    # LON -DDDMMSS (Degrees [-180:180] Minutes [00:60] Seconds [00:60])
    # <GPSLO,AA,-DDDMMSS,XXXX\r\n
    lon, addr = sock.recvfrom(1024)
    # ALT -TTTT (Altitude relative to sea level [m])
    # <GPSAT,AA,-TTTT,XXXX\r\n
    alt, addr = sock.recvfrom(1024)
    coordinates = [int(lat[10:16].decode('ascii')), \
                   int(lon[10:17].decode('ascii')), \
                   int(alt[10:14].decode('ascii'))]
    return coordinates

def getHeading(IP, port):
    message = NMT_SerialEncode('?HDG,AA,0,XXXX')
    sock.sendto(message,(IP, port))
    # <HDG,AA,B,NNN.N,XXXX
    # B - 0 True Heading, 1 Magnetic Heading
    # NNN.N - Current heading in degrees positive from north rotated through east
    heading, addr = sock.recvfrom(1024)
    heading = float(heading[10:15].decode('ascii'))
    return heading

def getAuxPosition(IP, port):
    # This message reports the current polarization (in the GEO frame) of the positioner
    message = NMT_SerialEncode('?UXP,AA,XXXX')
    sock.sendto(message,(IP, port))
    # <UXPOS,AA,-BBB.BB,XXXX
    # -BBB.BB - The current auxiliarry axis position (in the pedestal reference frame) \
    # of the positioner
    aux, addr = sock.recvfrom(1024)
    aux = float(aux[10:16].decode('ascii'))
    return aux

def getSerial(IP, port):
    message = NMT_SerialEncode('?SN,AA,XXXX')
    sock.sendto(message,(IP, port))
    # <SN,AA,NNNNNN,XXXX\r\n
    # NNNNNN - Serial number
    serial, addr = sock.recvfrom(1024)
    serial = int(serial[7:13].decode('ascii'))
    return serial

def getSpeed(IP, port):
    message = NMT_SerialEncode('?GSPEED,AA, XXXX')
    sock.sendto(message,(IP, port))
    # <SSPEED,AA,BB, XXXX\r\n
    # BB - Speed (1-10)
    speed, addr = sock.recvfrom(1024)
    speed = int(speed[11:13].decode('ascii'))   
    return speed

def stepEl(start, end, stepSize, Tdwell, IP, port):
    position = int(start)
    while position < int(end):
        message = NMT_SerialEncode('>CMDEL,99,' + str(position) + ',')
        # Set position
        sock.sendto(message,(IP, port))
        ElPosition, addr = sock.recvfrom(1024)
        # Determine actual position
        actualEl = getElPosition(IP, port)
        # While the system is moving from old position to new position, wait
        # The system isnt 100% accurate so we need to have some margin for error
        while abs(actualEl - position) > 0.5:
            actualAz = getElPosition(IP, port)
        # Once moved, increment position by step size
        position = position + int(stepSize)
        print('Current position: %i' %position)
        heading = getHeading(IP, port)
        print('Current heading: %i' %heading)
        # Wait for recording (dwell time)
        time.sleep(float(Tdwell))

def stepAz(start, end, stepSize, Tdwell, IP, port):
    # Identical to stepEl function
    position = int(start)
    while position < int(end):
        message = NMT_SerialEncode('>CMDAZ,99,' + str(position) + ',')
        sock.sendto(message,(IP, port))
        AzPosition, addr = sock.recvfrom(1024)
        actualAz = getAzPosition(IP, port)
        while abs(actualAz - position) > 0.5:
            actualAz = getAzPosition(IP, port)
        position = position + int(stepSize)
        print('Current position: %i' %position)
        heading = getHeading(IP, port)
        print('Current heading: %i' %heading)
        time.sleep(float(Tdwell))

############################################################

for i in range(len(sys.argv)):
    try:
        # Help
        if sys.argv[i] == '-h' or len(sys.argv) == 1:
            printHelp()
            
        # Set Azimuth
        elif sys.argv[i] == '-setaz':
            setAz = sys.argv[i+1]
            setAzPosition(setAz, IP, port)

        # Set Elevation
        elif sys.argv[i] == '-setel':
            setEl = sys.argv[i+1]
            setElPosition(setEl, IP, port)
        
        # Get Azimuth
        elif sys.argv[i] == '-getaz':
            actualAz = getAzPosition(IP, port)
            print('Azimuth position: %i' %actualAz)
        
        # Get Elevation
        elif sys.argv[i] == '-getel':
            actualEl = getElPosition(IP, port)
            print('Elevation position: %i' %actualEl)
        
        # Get GPS coordinates
        elif sys.argv[i] == '-gps':
            coordinates = getGPS(IP, port)
            print('Lat: %i' %coordinates[0])
            print('Lon: %i' %coordinates[1])
            print('Alt: %i' %coordinates[2])
        
        # Get Heading
        elif sys.argv[i] == '-deg':
            heading = getHeading(IP, port)
            print('Heading: %i' %heading)
        
        # Get Serial number
        elif sys.argv[i] == '-s':
            serial = getSerial(IP, port)
            print(serial)
        
        # Set Target latitude
        elif sys.argv[i] == '-lat':
            # lat = 'LLMMSS'
            # LLL - Degrees [-90:90]
            # MM - Minutes [00:60]
            # SS - Seconds [00:60]
            # lat = '1234567'
            lat = sys.argv[i+1]
            setTargetLat(lat, IP, port)
        
        # Set Target longitude
        elif sys.argv[i] == '-lon':
            # lon = 'LLLMMSS'
            # LLL - Degrees [-180:180]
            # MM - Minutes [00:60]
            # SS - Seconds [00:60]
            # lon = '12345678'
            lon = sys.argv[i+1]
            setTargetLon(lon, IP, port)
        
        # Set Target altitude
        elif sys.argv[i] == '-alt':
            # alt = 'BBBB'
            # BBBB - m above sea level
            alt = sys.argv[i+1]
            setTargetAlt(alt, IP, port)
        
        # Calibrate system
        elif sys.argv[i] == '-c':
            calibration = calibrateCompas(IP, port)
            print('Calibration: %s' %calibration)
        
        # Set the motor speed
        elif sys.argv[i] == '-speed':
            speed = sys.argv[i+1]
            setSpeed(speed, IP, port)
            speed = getSpeed(IP, port)
            print('Speed (1-10): %i' %speed)

        # Send a stop command to stop the system
        elif sys.argv[i] == '-stop':
            stop(IP, port)

        # Step from one point to another for calibration
        elif sys.argv[i] == '-stepaz':
            start = sys.argv[i+1]
            end = sys.argv[i+2]
            stepSize = sys.argv[i+3]
            Tdwell = sys.argv[i+4]
            stepAz(start, end, stepSize, Tdwell, IP, port)

        # Step from one point to another for calibration
        elif sys.argv[i] == '-stepel':
            start = sys.argv[i+1]
            end = sys.argv[i+2]
            stepSize = sys.argv[i+3]
            Tdwell = sys.argv[i+4]
            stepEl(start, end, stepSize, Tdwell, IP, port)

        # Stow system
        elif sys.argv[i] == '-stow:':
            stow(IP, port)

        # Gets the aux data from the system
        elif sys.argv[i] == '-aux':
            aux = getAuxPosition(IP, port)
            print(aux)
        
        else:
            continue

    except Exception as e:
        print('ERROR')
        print(e)

# Need to clean up open socket
sock.close()
